import Vue from 'vue'
import Router from 'vue-router'
import Lightbox from '@/components/lightbox/Lightbox'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Lightbox',
      component: Lightbox
    }
  ]
})
